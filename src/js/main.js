$(window).on('load', () => $('.preloader').delay(500).fadeOut('slow'));

$('.btn-menu-mobile').click(function() {
    $('.menu').toggle(300);
    $(this).toggleClass('isOpen');
});
$('.btn-top').click(function() {
    $('html, body').animate({ scrollTop: 0 }, 500);
});
$(window).scroll(function() {
    let isShowBtn = $('.feature').offset().top;
    if ($(this).scrollTop() >= isShowBtn) {
        $('.btn-top').css('opacity', '1');
    } else {
        $('.btn-top').css('opacity', '0');
    }
});