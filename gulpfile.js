'use strict';

const gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    removeComments = require('gulp-remove-html-comments'),
    postcss = require('gulp-postcss'),
    minify = require("gulp-csso"),
    sass = require('gulp-sass'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    autoprefixer = require('autoprefixer'),
    del = require('del'),
    browserSync = require('browser-sync').create(),
    source = 'src/',
    build = 'public';


gulp.task('html', function() {
    return gulp.src(source + '**/*.html')
        .pipe(gulp.dest(build))
        .pipe(removeComments())
        .pipe(browserSync.stream());
})
gulp.task('style', function() {
    return gulp.src(source + 'css/style.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss([autoprefixer({
            grid: true,
            overrideBrowserslist: ['last 2 versions', 'ie 6-8', 'Firefox > 20']
        })]))
        .pipe(minify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(build + '/css'))
        .pipe(browserSync.stream());
})
gulp.task('script', function() {
    return gulp.src(source + 'js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(babel())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(build + '/js'))
        .pipe(browserSync.stream());
})
gulp.task('imagemin', function() {
    return gulp.src(source + 'img/**/*.{png,jpg,jpeg}')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5
        }))
        .pipe(gulp.dest(build + '/img'))
        .pipe(browserSync.stream());
})
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './' + build
        }
    })
    gulp.watch(source + '**/*.html', gulp.series('html'));
    gulp.watch(source + 'css/**/*.{scss,sass}', gulp.series('style'));
    gulp.watch(source + 'js/**/*.js', gulp.series('script'));
})
gulp.task('copy', function() {
    return gulp.src([
            source + 'fonts/**/*.{otf,ttf,eot,woff,woff2}',
            source + '**/*.svg',
            source + '**/*.{mp3, ogg, wav}',
            source + '**/*.{mp4, avi}',
            source + '**/*.{doc,docx,txt,pdf}'
        ], {
            base: './' + source
        })
        .pipe(gulp.dest(build));
})
gulp.task('clear', function() {
    return del(build);
})

gulp.task('public-files', gulp.series('clear', 'html', 'style', 'script', 'imagemin', 'copy', function(done) {
    done();
}))

gulp.task('do', gulp.series('public-files', 'browser-sync', function(done) {
    done();
}))